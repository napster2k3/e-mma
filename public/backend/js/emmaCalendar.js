/**
 * Guillaume Lambert - <guillaume1.lambert@epitech.eu>
 * Project: E-MMA
 */
 $(document).ready(function(){
/*---------------------------------------------
#   Calendrier fullCalendar initialisation
#
------------------------------------------------*/
var Calendrier = $('#Calendrier');
Calendrier.fullCalendar({
  monthNames:['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
  monthNamesShort:['janv.','févr.','mars','avr.','mai','juin','juil.','août','sept.','oct.','nov.','déc.'],
  dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
  dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
  titleFormat: {
    month: 'MMMM yyyy',
    week: "d[ MMMM][ yyyy]{ - d MMMM yyyy}",
    day: 'dddd d MMMM yyyy'
  },
  columnFormat: {
    month: 'ddd',
    week: 'ddd d',
    day: 'dddd dd/M'
  },
  axisFormat: 'H:mm', 
  timeFormat: {
    '': 'H:mm', 
    agenda: 'H:mm{ - H:mm}'
  },
  firstDay:1,
  buttonText: {
    today: 'aujourd\'hui',
    day: 'jour',
    week:'semaine',
    month:'mois'
  }, 
  header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek,agendaDay'
  },
  defaultView: 'agendaDay',
  allDayDefault : false,
        // GET ALL EVENT
        events: '/admin/event/event',
        /*-------------------------------
        # ADD EVENT 
        --------------------------------*/
        selectable: true,
        selectHelper: false,
        select: function(start, end, allDay) {        
         start = $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss");
         end = $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss");
         allDay = false;
         var mywhen = start + ' - ' + end;
         $('#modal_event #apptStartTime').val(start);
         $('#modal_event #apptEndTime').val(end);
         $('#modal_event #apptAllDay').val(allDay);
         $('#modal_event #mywhen').text(mywhen);
         $('#modal_event').modal('show');   
         $(document).on('click', '.close, .btn-cancel', function(){   
          $('#modal_event').modal('hide');
          document.getElementById("eventForm").reset();
          Calendrier.fullCalendar('unselect');
        });
         $('#Valid_event').off('click')
         $('#Valid_event').on('click', function(e){
          e.preventDefault();
                    var description = tinyMCE.get('description').getContent();
                    var adresse = $('#adresse').val();
                    var place = $('#place').val();
                    doSubmit(start , end , title , allDay  , description , place , adresse);
                  });

         function doSubmit(start , end , title  , description , adresse , place){
          var title = $('#title').val();
          var description = tinyMCE.get('description').getContent();
          var adresse = $('#adresse').val();
          var place = $('#place').val();
          $.ajax({
            url : '/admin/event/create',
            data: {'title' : title , 'start' : start , 'end' : end  , 'allDay' : false , 'description' : description , 'place_name': place , 'adresse' : adresse},
            method : 'POST',
            dataType : 'json'
          }).done(function(data){
            console.log(data);
            $('#modal_event').modal('hide');
                            Calendrier.fullCalendar('refetchEvents'); // Recharger les events
                          }).fail(function(jqXHR , textStatus){
                           console.log(jqXHR);
                         });    
                          Calendrier.fullCalendar('unselect');
                        }       
                      },

        /*---------------------------
        # EDIT EVENT ON THE FLOW
        --------------------------*/
        editable: true,
        eventDrop: function(event , dayDelta  , minDelta  , allDay) {
         var id_user = event.id_user;
         $('#confirmModal').modal('show');
         $('.confirmAction').off('click')
         $('.confirmAction').on('click', function(e){
          var dataBool = $(this).attr('data-bool');
          e.preventDefault();
          verifAction(dataBool , event);
        });
         function verifAction(dataBool ,event)
         {
          $('#confirmModal').modal('hide');
          if(dataBool == 'false'){    
            $('#Planning').fullCalendar('refetchEvents');  
          } 
          else {
            updateEventDate(event , dayDelta , minDelta , allDay , id_user); 
          }

        }   
      },
      eventResize: function(event , dayDelta  , minDelta  , allDay) {
       var id_user = event.id_user;
       $('#confirmModal').modal('show');
       $('.confirmAction').off('click')
       $('.confirmAction').on('click', function(e){
        var dataBool = $(this).attr('data-bool');
        e.preventDefault();
        verifAction(dataBool , event);
      });
       function verifAction(dataBool ,event)
       { 
        $('#confirmModal').modal('hide');
        if(dataBool == 'false'){    
          $('#Planning').fullCalendar('refetchEvents');  
        } 
        else {
          updateEventDate(event , dayDelta , minDelta , allDay , id_user);   
        }
      }   
    },
        /*---------------------------------
        #   DELETE EVENT + EDIT EVENT NAME
        ----------------------------------*/
        eventClick : function(event){

          var id_user = event.id_user;
          var description = event.description;
          var place_name = event.place_name;
          var adresse = event.adresse;

          $('#modal_edit_event').modal('show');
          $('#titleEdit').val(event.title);      
          $('#Edit_event').off('click')
          $('#Edit_event').on('click', function(e){
            event.title = $('#titleEdit').val();
            event.description = tinyMCE.get('editDescription').getContent();
            console.log(event.description);
            event.place_name = $('#editPlace_name').val();
            event.adresse = $('#editAdresse').val();
            e.preventDefault();
            updateEvent(event , id_user);
          }); 
          $('#DeleteEvent').off('click')
          $('#DeleteEvent').on('click' , function(e){
            e.preventDefault();
            deleteEvent(event);     
          });
        }
      });


function deleteEvent(event)
{
 var data = {'id' : event.id , 'id_user' : event.id_user};              
 $.ajax({
  url: '/admin/event/delete',
  data: data,
  type: "POST",
  dataType : 'json'
}).done(function(){
  $('#modal_edit_event').modal('hide');
  Calendrier.fullCalendar('refetchEvents');
}).fail(function(jqXHR , textstatus){
  $('#modal_edit_event').modal('hide');
  $('#alertModal').modal('show');
  Calendrier.fullCalendar('refetchEvents'); 
});
}

function updateEvent(event ,  id_user)
{

  var data = {'title' : event.title , 'id' : event.id  , 'id_user' : id_user , 'description' : event.description , 'place_name' : event.place_name , 'adresse' : event.adresse};        
  $.ajax({
    url: '/admin/event/update',
    data: data,
    type: "POST",
    dataType : 'json'
  }).done(function(){
    $('#modal_edit_event').modal('hide');
    Calendrier.fullCalendar('refetchEvents'); 
  }).fail(function(jqXHR , textStatus){
    $('#modal_edit_event').modal('hide');
    $('#alertModal').modal('show');
    Calendrier.fullCalendar('refetchEvents'); 
  });
}

function updateEventDate(event , dayDelta , minDelta , allDay , id_user)
{
  var newStart = $('#Calendrier').fullCalendar('formatDate', event.start, "yyyy-MM-dd HH:mm:ss");
  var newEnd = $('#Calendrier').fullCalendar('formatDate', event.end, "yyyy-MM-dd HH:mm:ss");
 var data = {'dayDelta' : dayDelta , 'minDelta' : minDelta , 'title':event.title ,'id' : event.id , 'start' :newStart , 'end' : newEnd , 'id_user' : id_user};
 $.ajax({
  url: '/admin/event/update-date',
  data: data,
  type: "POST",
  dataType : 'json'
}).done(function(data){
Calendrier.fullCalendar('refetchEvents'); 
}).fail(function(jqXHR , textstatus){
  console.log(jqXHR);
 $('#alertModal').modal('show');
 Calendrier.fullCalendar('refetchEvents'); 
});
} 
});