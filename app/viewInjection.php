<?php

/**
 * Inject Logged User Data To View (Admin Only ATM)
 */


View::composer('layouts.admin', function($view)
{
    $auth = Auth::user();
    $auth->dateStr = App\Models\Timeline::dateToStr(strtotime($auth->created_at));
    $view->with('currentUser', $auth);
});

View::composer('layouts.master', function($view) {

    $auth = Auth::user();
    $view->with('currentUser', $auth);

});

?>