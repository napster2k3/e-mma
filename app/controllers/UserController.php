<?php

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

use App\Models\User;
use Toddish\Verify;

class UserController extends BaseController
{

    public function getAccount()
    {
        $user = Auth::user();
        return View::make('user.account', array("currentUser" => $user));
    }

    public function postPassword_update()
    {
        $user = Auth::user();
        $test = Input::all();
        if(empty($test['pw']) || empty($test['new']) || empty($test['new_confirmation'])) {
            return Redirect::to('/user/account')->with('error', true);
        } else {
            $t = Hash::make('admin');
            if (Hash::check($test['pw'], $user->password))
            {
                die('work');
            }
        }
    }

    /**
     * Upload Avatar Method
     * @return mixed
     */
    public function postAvatar()
    {
        if(Input::hasFile('avatar')) {
            $file = Input::file('avatar');
            if($file->isValid()) {
                if(in_array($file->getMimeType(), array("image/png", "image/jpeg"))) {
                    $ext = $file->guessExtension();
                    $availableExt =  array("jpg", "jpeg", "png");
                    if($ext != NULL && in_array($ext, $availableExt)) {
                        $user = Auth::user();
                        foreach($availableExt as $ex) {
                            $url = public_path() . '/users_avatar/' . $user->id . '.' . $ex;
                            if(file_exists($url)) {
                                unlink($url);
                            }

                        }
                        try {
                            $file->move(public_path() . '/users_avatar/', $user->id . '.' . $ext);
                            $user->avatar = "/users_avatar/" . $user->id . "." . $ext;
                            $user->save();
                            return Redirect::to('/user/account');

                        } catch(Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                            die('upload error | contact administrator');
                        }
                    }
                }
            }
        }
    }

    /**
     * registerAction method
     * @return mixed
     */
    public function getRegister()
	{
        if(!\Auth::check()) {
            return View::make('user.register');
        }
	}

    /**
     * Logout Action
     * @return mixed
     */
    public function getLogout()
    {
        Auth::logout();
        return Redirect::to('/');
    }

    public function getLogin()
    {
        if(!\Auth::check())
            return View::make('user.login');
    }

    /**
     * postLogin Process Action
     * @return mixed
     */
    public function postLogin()
    {
        $user = new User;
        $input = Input::all();
        $validator = Validator::make($input, $user->getRules('login'));
        if($validator->fails()) {
            return Redirect::to('user/login')->withErrors($validator);
        } else {

            try {
                Auth::attempt(array("email" => $input['email'], "password" => $input['pw']));
                return Redirect::to('/');
            }

            catch(Verify\UserNotFoundException $e) {
                $error = "Mauvais Identifiant";
            }

            catch(Verify\UserDeletedException $e) {
                $error = "Votre compte a été supprimé !";
            }

            catch(Verify\UserUnverifiedException $e) {
                $error  = "Votre compte n'est pas encore vérifié !";
            }

            catch(Verify\UserPasswordIncorrectException $e) {
                $error = "Mauvais Identifiant";
            }

            catch(Verify\UserDisabledException $e) {
                $error = "Votre compte a été désactivé";
            }

            $error = isset($error) ? $error : null;
            return Redirect::to('user/login')->with('error', $error);

        }

    }

    /**
     * Post Register Action
     * @return mixed
     */

    public function postRegister()
    {
        $user = new User();
        $input = Input::all();
        $validator = Validator::make($input, $user->getRules('register'));

        if($validator->fails()) {
            return Redirect::to('user/register')->withErrors($validator)->withInput();
        } else {
            $user->email = $input['email'];
            $user->password = $input['pw'];
            $user->verified = 0;
            $user->disabled = 0;
            $user->motivation = $input['motivation'];
            $user->state = $input['state'];
            $user->entity = $input['entity'];
            $user->name = $input['name'];
            $user->surname = $input['surname'];
            $user->username = $input['username'];
            $user->save();
            $user->roles()->sync(array(1));
            return Redirect::to('user/register')->with('success', true);
        }
    }
}
