<?php

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

class AdminCMSController extends AdminBaseController
{
    public function getIndex()
    {
        return View::make('admin.cms.index', array("pages" => Page::all()));
    }

    public function getNew()
    {
        return View::make('admin.cms.new');
    }

    public function getEdit($id)
    {
        return View::make('admin.cms.edit', array("page" => Page::findOrFail($id)));
    }

    public function postEdit($id)
    {
        $input = Input::all();
        $page = Page::findOrFail($id);
        $page->name = $input['name'];
        $page->title = $input['title'];
        $page->slug = $input['slug'];
        $page->content = $input['content'];
        $page->roles = implode(",", $input['roles']);
        $page->save();
        return Redirect::to('/admin/cms')->with('edited', 'true');
    }

    public function getDelete($id)
    {
        $page = Page::findOrFail($id);
        $page->delete();
        return Redirect::to('/admin/cms')->with('deleted', true);
    }

    public function postNew()
    {
        $input = Input::all();
        Page::create(
            array(
                "name" => $input['name'],
                "title" => $input['title'],
                "slug" => $input['slug'],
                "content" => $input['content'],
                "roles" => implode(",", $input['roles'])
            )
        );

        return Redirect::to('/admin/cms')->with('created', true);
    }
}