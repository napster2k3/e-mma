<?php

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

use App\Models\News;
use App\Models\User;
use Toddish\Verify\Models\Role;

class AdminUserController extends AdminBaseController
{
    public function getIndex()
    {
        $users = User::where('verified', '=', 1)->paginate(20);
        $userDisabled = User::where('verified', '=', 0)->paginate(20);
        return View::make('admin.users.index', array('users' => $users, "disabledUsers" => $userDisabled));
    }

    public function postDisable($id)
    {
        $input = Input::all();
        $u = User::find($id);
        $u->disabled = (int)isset($input['disabled']);
        $u->disabled_reason = $input['disabled_reason'];
        $u->save();
        return Redirect::to('/admin/users/profile/' . $id)->with('edited', true);
    }

    public function postRole($id)
    {
        $input = Input::all();
        User::find($id)
              ->roles()
              ->sync(array($input['user_role']));
        return Redirect::to('/admin/users/profile/' . $id)->with('edited', true);
    }

    public function postEdit($id)
    {
        $user = User::find($id);
        if($user == NULL) {
            return Redirect::to('/admin/users');
        } else {
            $input = Input::all();
            unset($input['_token']);
            foreach($input as $key => $value)
                $user->$key = $value;
            $user->save();
            return Redirect::to('/admin/users/profile/' . $id)->with('edited', true);
        }
    }

    public function getProfile($id, $route = null)
    {
        $user = User::find($id);
        if(isset($route)) {
            switch($route) {
                case 'approve' :
                    if($user->approveUser($user))
                        return \Redirect::to('/admin/users/profile/'.$id)->with('approved', true);
                    else 
                        return \Redirect::to('/admin/users/profile/'.$id);
                break;

                case 'delete' :
                    if($user->is('Super Admin')) {
                        $user->delete();
                        return \Redirect::to('/admin/users/'.$id)->with('deleted', true);

                    } else {
                        return \Redirect::to('/admin/users/profile/'.$id);
                    }
                break;

                case 'edit' :
                    $edit = true;
                break;
            }
        }

        if($user != NULL) {
            return View::make('admin.users.profile', array("user" => $user, "edit" => isset($edit), "roles" => Role::all()));
        } else {
            return Redirect::to('/admin/users');
        }
    }
}