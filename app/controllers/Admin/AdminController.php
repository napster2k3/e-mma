<?php

namespace App\Controller\Admin;

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

use App\Models\Timeline;

class AdminController extends \AdminBaseController
{
    public function getDashboard()
    {
        return \Redirect::to('/admin/timeline');
    }

    public function getTimeline()
    {
        $timeline = Timeline::take(50)->orderBy("id", "DESC")->get();
        foreach($timeline as $line) {
            $line->ago = Timeline::dateToStr(strtotime($line->updated_at));
        }

        return \View::make("admin.timeline", array("timeline" => $timeline));
    }
}