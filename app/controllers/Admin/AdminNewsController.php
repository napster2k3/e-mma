<?php

namespace App\Controller\Admin;

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

use App\Models\News;


class AdminNewsController extends \AdminBaseController
{
    public function getIndex()
    {
        $news = News::paginate(10);
        return \View::make('admin.news.index', array("news" => new News, "listNews" => $news));
    }

    public function getEdit($id)
    {
        return \View::make('admin.news.edit', array("news" => News::findOrFail($id)));
    }

    public function postEdit($id)
    {
        $input = \Input::all();
        $news = News::find($id);
        $news->title = $input['title'];
        $news->content = $input['content'];
        $news->slug = $input['slug'];
        $news->published = !isset($input['published']) ? 0 : 1;
        $news->save();
        return \Redirect::to('/admin/news')->with('updated', true);
    }

    public function getDelete($id)
    {
        $news = News::find($id);
        $news->delete();
        return \Redirect::to('/admin/news')->with('deleted', true);
    }

    public function postCreate()
    {
        $input = \Input::all();
        unset($input['_token']);
        $input['author_id'] = \Auth::user()->id;
        News::create($input);
        return \Redirect::to('/admin/news')->with('created', 'success');
    }
}