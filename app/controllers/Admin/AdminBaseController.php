<?php

class AdminBaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

    protected $layout = 'layouts.admin';

    public function __construct()
    {
        $this->beforeFilter(function() {
            if(!\Auth::check())
                return Redirect::to('user/login');
            else {
                if(!\Auth::user()->is('Super Admin'))
                    App::abort(403, 'You are not allowed to visist this resource !');
            }
        });
    }

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
