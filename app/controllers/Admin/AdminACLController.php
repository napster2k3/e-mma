<?php

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

use App\Models\User;
use Toddish\Verify\Models\Role;
use Toddish\Verify\Models\Permission;

class AdminACLController extends AdminBaseController
{
    public function getIndex()
    {
        $users = User::where('disabled', '=', 0)->paginate(1)->get();
        return View::make('admin.users.index', array('users' => $users));
    }

    public function getPermissions()
    {
        return View::make('admin.acl.permission', array('perms' => Permission::orderBy('created_at')->paginate(20)));
    }
}