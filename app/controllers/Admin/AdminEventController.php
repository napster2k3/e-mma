<?php  
/**
 * Guillaume Lambert - <guillaume1.lambert@epitech.eu>
 * Project: E-MMA
 */

use App\Models\Event;

class AdminEventController extends \AdminBaseController
{
	public function getIndex()
	{
		return \View::make('admin.events.index');
	}

	public function getEvent()
	{
		if(Request::ajax())
		{
			$event = Event::all();
			return Response::json($event);
		}
		
	}

	public function postCreate()
	{
		if( Request::ajax()){
			$input = \Input::all();
			unset($input['_token']);
			$input['author_id'] = \Auth::user()->id;
			$input['author_email'] = \Auth::user()->email;
			$data = Event::create($input);
			return Response::json($data);
		}
	}

	public function postUpdate()
	{
		$input = \Input::all();
		$event = Event::find($input['id']);
		$event->title = $input['title'];
		$event->place_name = $input['place_name'];
		$event->adresse = $input['adresse'];
		$event->description = $input['description'];
		$event->save();
		return Response::json($event);
	}

	public function getUpdateDate()
	{
		echo "mange ma banane";
	}

	public function postUpdateDate()
	{
		$input = \Input::all();

		$newStart = $input['start'];
		$newEnd = $input['end'];
		$event = Event::find($input['id']);
		$event->start = $newStart;
		$event->end = $newEnd;
		$event->save();
		return Response::json($event);
	}

	public function postDelete()
	{
		$input = \Input::all();
		$event = Event::find($input['id']);
		$event->delete();
		return Response::json($event);
	}
}