<?php

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

use App\Models\News;

class NewsController extends BaseController
{
    public function getIndex()
    {
            $news = News::orderBy('id', 'DESC')->get();
            foreach($news as $val)
                $val->splitContent();
            return View::make('news.index', array("news" => $news));
    }

    public function showNews($slug)
    {
        $news = News::where("slug", "=", $slug)->firstOrFail();
        return View::make('news.show', array("news" => $news));
    }
}
