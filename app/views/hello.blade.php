@extends('layouts.master')

@section('content')
<div class="col-md-8 col-sm-8" style="margin-bottom: 20px;">
    <div class="box-title-container">
        <h3 class="box-title">
            <span><img src="{{ asset('front/css/images/qui.png') }}" class="icon"/> qui sommes nous ?</span>
        </h3>
        <div class="box box_headline" style="overflow: hidden">
            <a href="#"><img width:"1475" height="850" src="{{ asset('front/css/images/article_site_552014.png') }}" class="img_thumbnail wp-post-image" alt="article_site_552014"></a>
            <div class="separator"></div>
            <div class="caption"></div>
            <h4><a href="#">Un tremplin pour les filles à Epitech</a></h4>
            <p>L’idée d’E-mma est née d’un constat évident : même si depuis 15 ans, l’informatique se démocratise dans sa volonté de parité, il reste toujours plutôt masculin tant en terme de population que d’image. Epitech est aujourd’hui le reflet de cette réalité. D’ailleurs, en 2013 les filles ne représentent toujours que 4,2% de la population étudiante à l’école, toutes régions confondues. Ce « déséquilibre » filles/garçons peut questionner certaines étudiantes, les désarçonner et se transformer en barrage. Pour éviter cela, E-mma propose aux jeunes femmes qui le souhaitent un accompagnement personnalisé tout au long de leur scolarité afin d’optimiser leur parcours […]</p>
            <span class="moretext"><a href="#">Lire la suite</a></span>
            <div style="clear: both"></div>
        </div>

    </div>
</div>
@stop