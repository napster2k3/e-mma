@extends('layouts.admin')
@section('pageName')
Profile de {{ HTML::entities( $user->surname . " " . $user->name ) }}
@stop

@section('content')
    @if(Session::has('approved'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> L'utilisateur a été approuvé ! Un email lui a été remis.
    </div>
    @endif
    @if(Session::has('edited'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> L'utilisateur a été mis à jour !
    </div>
    @endif
    <div class="row">
        <div class="col-md-3">
            @if(!$user->verified)
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid bg-red">
                        <div class="box-header">
                            <h3 class="box-title">L'utilisateur n'est pas approuvé !</h3>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
              <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-header">
                        <h3 class="box-title"></h3>

                    </div>
                    <div class="box-body">
                       <div style="text-align:center">
                           <img src="{{ asset($user->avatar) }}" alt="Profile Picture" class="img-circle" style="height:128px; width:128px">
                       </div>
                        <br>


                    </div><!-- /.box-body -->
                </div>
               </div>
            </div>

        </div>
        @if($edit)
            @include('admin.users.edit')
        @else
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Informations</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab">News</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab">Roles</a></li>
                    <li class="pull-right dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-gear"></i> 
                        </a>
                        <ul class="dropdown-menu">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/admin/users/profile/{{ $user->id }}/edit">Editer</a></li>
                            @if(Auth::user()->is('Super Admin'))
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/admin/users/profile/{{ $user->id }}/delete">Supprimer</a></li>
                            @endif
                        </ul>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <dl class="dl-horizontal">
                            <dt>#</dt>
                            <dd>{{ $user->id }}</dd>
                            <dt>Email:</dd>
                            <dd>{{ HTML::entities($user->email) }}</dd>
                            <dt>Login:</dt>
                            <dd>{{ HTML::entities($user->username) }}</dd>
                            <dt>Nom:</dt>
                            <dd>{{ HTML::entities($user->name) }}</dd>
                            <dt>Prénom:</dt>
                            <dd>{{ HTML::entities($user->surname) }}</dd>
                            <dt>Status:</dt>
                            <dd>{{ HTML::entities($user->state) }}</dd>
                            <dt>Date d'inscription:</dt>
                            <dd>{{ $user->created_at }}</dd>
                            <dt>Entité:</dt>
                            <dd>{{ HTML::entities($user->entity != NULL ? $user->entity : 'N/A') }}</dd>
                            <dt>Motivation:</dt>
                            <dd>{{ HTML::entities($user->motivation != NULL ? $user->entity : 'N/A') }}</dd>
                        </dl>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">

                        <table class="table table-striped">
                            <tbody><tr>
                                <th style="width: 10px">#</th>
                                <th>Titre</th>
                                <th>Date de publication:</th>
                                <th style="width: 40px">Gestion</th>
                            </tr>
                            @foreach($user->news as $key)
                            <tr>
                                <td>{{ $key->id }}</td>
                                <td>{{ HTML::entities($key->title) }}</td>
                                <td>
                                   {{ $key->created_at }}
                                </td>
                                <td><a href="/admin/news/edit/{{ $key->id }}" class="btn bg-aqua">Gérer la news</a></td>
                            </tr>
                            @endforeach

                            </tbody></table>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        <dl class="dl-horizontal">
                            <dt>Role:</dt>
                            <dd>{{ $user->roles()->first()->name }}</dd>

                        </dl>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
            @if(!$user->verified)
            <div class="row">
                <div class="col-md-12">
                    <a href="/admin/users/profile/{{ $user->id }}/approve" class="btn bg-olive pull-right">Accepter la demande</a>
                </div>
            </div>
            @endif
        </div>
        @endif
    </div>
@stop