<div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Informations</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab">Roles</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab">Désactiver</a></li>
                    <li class="pull-right dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-gear"></i> 
                        </a>
                        <ul class="dropdown-menu">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/admin/users/profile/{{ $user->id }}/edit">Editer</a></li>
                            @if(Auth::user()->is('Super Admin'))
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/admin/users/profile/{{ $user->id }}/delete">Supprimer</a></li>
                            @endif
                        </ul>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                    	<div class="row">
                    		<div class="col-md-6">
		                    	{{ Form::model($user, array("url" => "/admin/users/edit/$user->id")) }}
				                <div class="form-group">
				                	{{ Form::label('Email')}}
				                    {{ Form::text('email', null, array("class" => "form-control")) }}
				                </div>
				            	 <div class="form-group">
				                	{{ Form::label('Login')}}
				                    {{ Form::text('username', null, array("class" => "form-control")) }}
				                </div>
				                <div class="form-group">
				                	{{ Form::label('Nom')}}
				                    {{ Form::text('name', null, array("class" => "form-control")) }}
				                </div>
				             	<div class="form-group">
				                	{{ Form::label('Prénom')}}
				                    {{ Form::text('surname', null, array("class" => "form-control")) }}
				                </div>
				                <div class="form-group">
				                	{{ Form::label('Status')}}
				                	{{ Form::select(
                                        'state',
                                        array('student' => 'Etudiant', 'external' => 'Externe', 'association' => 'Association', 'company' => 'Entreprise'),
                                        null,
                                        array("class" => "form-control")
                                    ) }}
				                </div>
				                <div class="form-group">
				                	{{ Form::label('Entité')}}
				                    {{ Form::text('entity', null, array("class" => "form-control")) }}
				                </div>
				                <div class="form-group">
				                	{{ Form::submit('Modifier', array("class" => "btn btn-info"))}}
				                </div>
				                {{ Form::close() }}
			            	</div>
		            	</div>
                    </div><!-- /.tab-pane -->
                
                    <div class="tab-pane" id="tab_2">
                        {{ Form::model($user, array("url" => "/admin/users/role/$user->id")) }}
                        <div class="form-group">
                            {{ Form::select(
                                'user.role',
                                array('1' => 'User', '2' => 'Super Admin', '3' => 'Bureau'),
                                $user->roles()->first()->id,
                                array("class" => "form-control")
                            ) }}
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Modifier le role', array("class" => "btn btn-danger")) }}
                        </div>
                        {{ Form::close() }}
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        {{ Form::model($user, array("url" => "/admin/users/disable/$user->id")) }}
                            <div class="form-group">
                                {{ Form::label('Utilisateur désactivé') }}
                                {{ Form::checkbox('disabled') }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('Raison') }}
                                {{ Form::textarea('disabled_reason') }}
                            </div>
                            <div class="form-group">
                                {{ Form::submit('Mettre à jour', array("class" => "btn btn-success")) }}
                            </div>
                        {{ Form::close() }}
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
            @if(!$user->verified)
            <div class="row">
                <div class="col-md-12">
                    <a href="/admin/users/profile/{{ $user->id }}/approve" class="btn bg-olive pull-right">Accepter la demande</a>
                </div>
            </div>
            @endif

        </div>