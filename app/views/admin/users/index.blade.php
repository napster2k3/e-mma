@extends('layouts.admin')
@section('pageName')
Users
@stop

@section('content')
@if(Session::has('deleted'))
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Success!</b> L'utilisateur a été supprimé !
</div>
@endif
<div class="row">

    <div class="col-md-7">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">List</h3>
                <div class="box-tools pull-right">
                    <div class="label bg-aqua">Actif</div>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody><tr>
                        <th>Email</th>
                        <th>Nom</th>
                        <th>Status</th>
                        <th style="width: 40px">Gestion</th>
                    </tr>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ HTML::entities($user->email) }}</td>
                        <td>{{ HTML::entities($user->surname . " " . $user->name) }}</td>
                        <td>{{ HTML::entities($user->state) }}</td>
                        <td><a class="btn btn-info" href="/admin/users/profile/{{ $user->id }}">Gérer l'utilisateur</a></td>
                    </tr>
                    @endforeach

                    </tbody></table>
            </div><!-- /.box-body -->
            <div class="box-footer" style="padding-top:0px; padding-bottom:0px">
                <div class="pull-right">
                    {{ $users->links(); }}
                </div>
            </div><!-- /.box-footer-->
        </div>
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            {{ sizeof($users) }}<sup style="font-size: 20px"></sup>
                        </h3>
                        <p>
                            Utilisateurs Actif
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>
                            {{ sizeof($disabledUsers) }}<sup style="font-size: 20px"></sup>
                        </h3>
                        <p>
                            Utilisateurs en attente
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- Danger box -->
                <div class="box box-solid box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Pending Request</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-danger btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-danger btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody><tr>
                                <th>Email</th>
                                <th>Nom</th>
                                <th>Status</th>
                                <th style="width: 40px">Gestion</th>
                            </tr>
                            @foreach($disabledUsers as $user)
                            <tr>
                                <td>{{ HTML::entities($user->email) }}</td>
                                <td>{{ HTML::entities($user->surname . " " . $user->name) }}</td>
                                <td>{{ HTML::entities($user->state) }}</td>
                                <td><a class="btn btn-danger" href="/admin/users/profile/{{ $user->id }}">Gérer l'utilisateur</a></td>
                            </tr>
                            @endforeach

                            </tbody></table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </div>
</div>
@stop