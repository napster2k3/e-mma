@extends('layouts.admin')

@section('pageName')
Events
@stop

@section('content')
<?php if(Session::has('created')) { ?>
	<div class="alert alert-success alert-dismissable">
		<i class="fa fa-check"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Success!</b> The event has been added !
	</div>
	<?php } ?>
	<?php if(Session::has('updated')) { ?>
	<div class="alert alert-success alert-dismissable">
		<i class="fa fa-check"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Success!</b> The event has been updated !
	</div>
	<?php } ?>
	<?php if(Session::has('deleted')) { ?>
	<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Success!</b> The event has been removed !
	</div>
	<?php } ?>
	<div class="row">
		<div class="col-md-12">
			<div id="Calendrier">

			</div>
		</div><!-- /.col-->
	</div>
	<!-- MODAL CREATE EVENT -->
	<div class="modal fade" id="modal_event" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Créer un Evènement</h4>
				</div>
				<div class="modal-body">
					<form name="eventForm" id="eventForm" action="" method='POST'>
						<div class="form-group">
							<label for='title'>Titre</label>
							<input class='form-control' type='text' id="title">
						</div>
						<div class="form-group">
							<label for="description">Description</label>
							<textarea id='description' rows="3"></textarea>
						</div>
						<div class="form-group">
							<label for='place'>Lieu</label>
							<input class='form-control' id='place' name='place' type="text">
						</div>
						<div class="form-group">
							<label for='adresse'>Adresse</label>
							<input id="adresse" type="text"  name="adresse" class='form-control'>
						</div>
						<input type="hidden" id="apptStartTime">
						<input type="hidden" id="apptEndTime">
						<input type="hidden" id="apptAllDay">
					</form>
					<div class="well">
						<p class='text-info text-center' id="mywhen"></p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button id="Valid_event" type="button" class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN MODAL CREATE EVENT -->
	<!-- MODAL EDIT EVENT -->
	<div id="modal_edit_event" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-tint"></i> Editer un Evenement</h4>
				</div>
				<div class="modal-body">
					<form class="form" id="eventEditForm">
						<div class="form-group">
							<label class="control-label" for="titleEdit">Titre : </label>
							<input type="text" name="titleEdit" id="titleEdit" class="form-control">
						</div>
						<div class="form-group">
							<label for='editDescription'>Description</label>
							<textarea id='editDescription'></textarea>
						</div>
						<div class="form-group">
							<label for='editPlace_name'>Lieu</label>
							<input type='text' id='editPlace_name' name='editPlace_name' class="form-control">
						</div>
						<div class="form-group">
							<label for='editAdresse'>Adresse</label>
							<input type='text' id='editAdresse'name="editAdresse"  class="form-control">
						</div>

						<input type="hidden" id="apptStartTime">
						<input type="hidden" id="apptEndTime">
						<input type="hidden" id="apptAllDay">
						<div class="controls controls-row" id="mywhen">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" id="DeleteEvent" class="btn btn-danger btn-cancel pull-left"><i class="fa fa-thumbs-o-down"></i>  Supprimer</button>
					<button type="button" class="btn btn-danger btn-cancel" data-dismiss="modal"><i class="fa fa-thumbs-o-down"></i>  Annuler</button>
					<button id="Edit_event" type="button" class="btn btn-primary"><i class="fa fa-thumbs-o-up"></i>Valider</button>
				</div>
			</div>
		</div>
	</div>
	<!--FIN MODAL EDIT EVENT -->
	<!-- MODAL ALERT -->
	<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header modal-header-danger">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h1><i class="fa fa-cog fa-spin"></i> Attention</h1>
				</div>
				<div class="modal-body">
					<p class="lead">Vous n'avez pas les autorisations requises pour cette action !</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<!-- FIN MODAL ALERT -->
	<!-- MODAL CONFIRM -->
	<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header modal-header-info">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h1><i class="fa fa-cog fa-spin"></i> Attention</h1>
				</div>
				<div class="modal-body">
					<p class="lead">Veuillez confirmer cette action</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-cancel confirmAction" data-bool="false" data-dismiss="modal"><i class="fa fa-thumbs-o-down"></i>  Annuler</button>
					<button id="Action" type="button" class="btn btn-primary confirmAction" data-bool="true"><i class="fa fa-thumbs-o-up"></i>Valider</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
	<!-- FIN MODAL CONFIRM -->
	@stop