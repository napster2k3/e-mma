@extends('layouts.admin')

@section('pageName')
Timeline
@stop
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- The time line -->
        <ul class="timeline">
            <!-- timeline time label -->
            <li class="time-label">
                                    <span class="bg-orange">
                                        Historique: 2 mois
                                    </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            @foreach($timeline as $info)
            <li>
                <i class="fa {{ $info->icon }} {{ $info->color }}"></i>
                <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> Il y a {{ $info->ago }}</span>
                    <h3 class="timeline-header">{{ $info->user_id == null ? $info->title : '<a href="/admin/users/profile/' . $info->user->id .'">' . HTML::entities($info->user->username) .'</a> ' . $info->title }}</h3>
                    <div class="timeline-body">
                        {{ $info->content }}<br><br>
                    </div>

                </div>
            </li>
            @endforeach

            <!-- END timeline item -->
            <!-- timeline time label -->
            <!-- END timeline item -->
            <li>
                <i class="fa fa-clock-o"></i>
            </li>
        </ul>
    </div><!-- /.col -->
</div>

@stop