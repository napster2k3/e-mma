@extends('layouts.admin')
@section('pageName')
Gestion des permissions
@stop

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box box-danger">
            <div class="box-header">
                <i class="fa fa-lock"></i>
                <h3 class="box-title">Liste des permissions</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody><tr>
                        <th style="">Nom de la permission</th>
                        <th>Description</th>
                        <th>Date d'ajout</th>
                        <th>Gestion</th>
                    </tr>
                    @foreach($perms as $perm)
                    <tr>
                        <td>{{ HTML::entities($perm->name) }}</td>
                        <td>{{ HTML::entities($perm->description) }}</td>
                        <td>{{ $perm->created_at }}</td>
                        <td><a class="btn btn-info">Gérer la permission</a></td>
                    </tr>
                    @endforeach

                    </tbody></table>
            </div><!-- /.box-body -->
        </div>
        <div class="pull-right" style="margin-top:-30px">
            {{ $perms->links() }}
        </div>
    </div>
    <div class="col-md-6">

    </div>
</div>
@stop