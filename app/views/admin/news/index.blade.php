@extends('layouts.admin')

@section('pageName')
News
@stop

@section('content')
<?php if(Session::has('created')) { ?>
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Success!</b> The news has been added !
</div>
<?php } ?>
<?php if(Session::has('updated')) { ?>
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> The news has been updated !
    </div>
<?php } ?>
<?php if(Session::has('deleted')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> The news has been removed !
    </div>
<?php } ?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-danger">
            <div class="box-header">
                <i class="fa fa-bookmark"></i>
                <h3 class="box-title">Liste des news</h3>
                <div class="pull-right box-tools">
                    <button class="btn btn-danger btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /. tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-striped">
                    <tbody><tr>
                        <th style="width: 10px">#</th>
                        <th>Titre</th>
                        <th>Auteur</th>
                        <th>Progress</th>
                        <th style="width: 40px">%</th>
                        <th>Gestion</th>
                    </tr>
                    @foreach($listNews as $vNews)
                        <tr>
                            <td>{{{ $vNews->id }}}</td>
                            <td>{{ HTML::entities($vNews->title) }}</td>
                            <td>{{ HTML::entities($vNews->author->username) }}</td>
                            <td>
                                <div class="progress xs">
                                    <div class="progress-bar {{ $vNews->published == 1 ? 'progress-bar-success' : 'progress-bar-danger' }}" style="width: {{ $vNews->published == 1  ? '100' : '50' }}%"></div>
                                </div>
                            </td>
                            <td><span class="badge {{ $vNews->published == 1 ? 'bg-green' : 'bg-red' }}">{{ $vNews->published == 1 ? '100' : '50' }}%</span></td>
                            <td style="width:8%"><a class="btn btn-default btn-sm" href="/admin/news/edit/{{{ $vNews->id }}}">Editer</a></td>
                        </tr>
                    @endforeach
                </tbody></table>
                <div class="pull-right">
                    {{ $listNews->links(); }}
                </div>
            </div><!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header">
                <i class="fa fa-pencil"></i>
                <h3 class="box-title">Ajouter une News</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /. tools -->
            </div><!-- /.box-header -->
            <div class="box-body pad">
                {{ Form::model($news, array("url" => "/admin/news/create")) }}
                <div class="form-group">
                    {{ Form::text('title', null, array("class" => "form-control", "placeholder" => "Titre de la news...")) }}
                </div>
                <div class="form-group">
                    {{ Form::text('slug', null, array("class" => "form-control", "placeholder" => "Slug de la news...")) }}
                </div>
                <div class="form-group">
                    {{ Form::textarea('content') }}
                </div>
                <div class="form-group">
                    <label>En ligne: </label>
                    {{ Form::checkbox('published', '1', true) }}
                    <div class="pull-right">
                        {{ Form::submit('Ajouter', array("class" => "btn btn-info"))}}
                    </div>
                </div>
                {{ Form::close() }}

                <script type="text/javascript">
                    var $titre = $("input[name$='title']");
                    var $slug = $("input[name$='slug']");
                    $titre.on('keyup', function() {
                        $slug.val($titre.val().replace(/ /g, "-"));
                    })
                </script>

            </div>
        </div><!-- /.box -->
    </div><!-- /.col-->
</div>
@stop