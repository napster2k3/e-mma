@extends('layouts.admin')

@section('pageName')
Edition News #{{ $news->id }}
@stop

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Edit #{{ $news->id }}</h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /. tools -->
            </div><!-- /.box-header -->
            <div class="box-body pad">
                {{ Form::model($news, array("url" => "/admin/news/edit/" . $news->id)) }}
                <div class="form-group">
                    {{ Form::text('title', null, array("class" => "form-control", "placeholder" => "Titre de la news...")) }}
                </div>
                <div class="form-group">
                    {{ Form::text('slug', null, array("class" => "form-control", "placeholder" => "Slug de la news...")) }}
                </div>
                <div class="form-group">
                    {{ Form::textarea('content') }}
                </div>
                <div class="form-group">
                    <label>En ligne: </label>
                    {{ Form::checkbox('published', '1', true) }}
                    <div class="pull-right">
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::submit('Editer', array("class"=>"btn btn-info")) }}
                </div>

                <script type="text/javascript">
                    var $titre = $("input[name$='title']");
                    var $slug = $("input[name$='slug']");
                    $titre.on('keyup', function() {
                        $slug.val($titre.val().replace(/ /g, "-"));
                    })
                </script>

            </div>
        </div><!-- /.box -->
    </div><!-- /.col-->
    <div class="col-md-6">
        <div class="pull-right">
            <a class="btn btn-app" href="/admin/news/delete/{{$news->id}}">
                <i class="fa fa-ban"></i> Delete
            </a>
        </div>
    </div>
</div>


@stop