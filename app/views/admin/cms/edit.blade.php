@extends('layouts.admin')

@section('pageName')
CMS: Edit Page {{{ $page->name }}}
@stop

@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="box box-solid box-info">
            <div class="box-header">
                <h3 class="box-title">Paramètre de la page</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                {{ Form::model($page, array("url" => "/admin/cms/edit/$page->id")) }}
                <!-- text input -->
                <div class="form-group">
                    <label>Nom Générique</label>
                    {{ Form::text('name', null, array('class' => "form-control")) }}
                </div>
                <div class="form-group">
                    <label>Titre de la page<em><code>< title ></code></em></label>
                    {{ Form::text('title', null, array('class' => "form-control")) }}
                </div>
                <div class="form-group">
                    <label>Slug</label>
                    {{ Form::text('slug', null, array('class' => "form-control")) }}
                </div>

                <!-- select -->
                <div class="form-group">
                    <label>Roles:</label><br>
                    <label>Free Access</label>
                    {{ Form::checkbox('roles[]', '0') }}

                    <label>User</label>
                    {{ Form::checkbox('roles[]', '1') }}

                    <label>Super Admin</label>
                    {{ Form::checkbox('roles[]', '2') }}

                    <label>Bureau</label>
                    {{ Form::checkbox('roles[]', '3') }}
                    {{ Form::text('content', null, array("style" => "display:none;")) }}
                </div>

            </div><!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">Contenu de la page</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    {{ Form::textarea('content', null, array("class" => "textarea-no-styles", "style" => "width:100%; height:287px")) }}
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::submit('Mettre à jour la page', array("class" => "btn btn-success pull-right", "id" => "submitNew") ) }}
            </div>
        </div>
    </div>

    {{ Form::close() }}


</div>

<script type="text/javascript">

    var $titre = $("input[name$='name']");
    var $slug = $("input[name$='slug']");
    $titre.on('keyup', function() {
        $slug.val($titre.val().replace(/ /g, "-"));
    })

</script>


@stop