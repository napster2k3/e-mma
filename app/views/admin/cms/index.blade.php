@extends('layouts.admin')

@section('pageName')
    Content Managering System
@stop

@section('content')
<?php if(Session::has('created')) { ?>
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b>Success!</b> The cms page has been added !
    </div>
<?php } ?>

@if(Session::has('edited'))
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Success!</b> The cms page has been updated !
</div>
@endif

@if(Session::has('deleted'))
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Success!</b> The cms page has been deleted !
</div>
@endif

<div class="row">
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                <i class="fa fa-bookmark"></i>
                <h3 class="box-title">Liste des pages</h3>
                <div class="pull-right box-tools">
                    <button class="btn btn-success btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-success btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /. tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-striped">
                    <tbody><tr>
                        <th style="width: 10px">#</th>
                        <th>Titre</th>
                        <th>Slug</th>
                        <th>Date de Création</th>
                        <th>Gestion</th>
                    </tr>
                    @foreach($pages as $page)
                    <tr>
                        <td>{{ $page->id }}</td>
                        <td>{{{ $page->name }}}</td>
                        <td>/{{{ $page->slug }}}</td>
                        <td>{{{ $page->created_at }}}</td>
                        <td style="width:20%">
                            <a class="btn btn-default btn-sm" href="/admin/cms/edit/{{ $page->id }}">Editer</a>
                            <a class="btn btn-danger btn-sm" href="/admin/cms/delete/{{ $page->id }}">Supprimer</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody></table>
            </div><!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>
                    Create new
                </h3>
                <p>
                    CMS Page
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="/admin/cms/new" class="small-box-footer">
                Cliquez pour être redirigé !
            </a>
        </div>
    </div>
</div>

@stop