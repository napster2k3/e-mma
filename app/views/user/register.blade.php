@extends('layouts.master')

@section('pageName')
Inscription
@stop

@section('sidebar')
@parent

@stop

@section('content')

<div class="col-md-8 col-sm-8" style="margin-bottom: 20px;">
    @if(Session::has('success'))
    <div class="alert alert-success">
        Votre inscription a bien été prise en compte ! <br>Vous devez cependant attendre la validation de celle-ci par un membre E-mma
    </div>
    @endif
    <div class="box-title-container">
        <h3 class="box-title">
            <span><img src="{{ asset('front/css/images/actu_ico.png') }}" class="icon"/> Inscription</span>
        </h3>

        <div class="box">
            <div class="box_content">
                <ul>
                    <?php
                    foreach($errors->getMessages() as $msg)
                    {
                        ?><li><?= $msg[0]; ?></li><?php
                    }
                    ?>
                </ul>
                {{ Form::open(array('action' => 'UserController@postRegister')) }}
                <div class="form-group">
                {{ Form::label('email', 'Email')}}
                {{ Form::text('email', null, array("class" => "form-control")) }}
                </div>
                <div class="form-group">
                {{ Form::label('pw', 'Password')}}
                {{ Form::password('pw', array("class" => "form-control")) }}
                </div>
                <div class="form-group">
                {{ Form::label('pw_confirm', 'Confirmation')}}
                {{ Form::password('pw_confirm', array("class" => "form-control")) }}
                </div>
                <div class="form-group">
                {{ Form::label('username', 'Login (Pseudo)')}}
                {{ Form::text('username', null, array("class" => "form-control")) }}
                </div>
                <div class="form-group">
                {{ Form::label('name', 'Nom') }}
                {{ Form::text('name', null, array("class" => "form-control"))}}
                </div>
                <div class="form-group">
                {{ Form::label('surname', 'Prénom') }}
                {{ Form::text('surname', null, array("class" => "form-control"))}}
                </div>
                <div class="form-group">
                {{ Form::label('state', 'Status')}}
                {{ Form::select('state', array('student' => 'Etudiant', 'external' => 'Externe', 'association' => 'Association', 'company' => 'Entreprise'), null, array("id" => "state", "class" => "form-control")) }}
                </div>
                <div class="form-group" id="motivation">
                    {{ Form::label('motivation', 'Motivation')}}
                    {{ Form::textarea('motivation', null, array("class" => "form-control")) }}
                </div>
                <div class="form-group" id="entityName">
                    {{ Form::label('entity', 'Nom de l\'entité')}}
                    {{ Form::text('entity', null, array("class" => "form-control")) }}
                </div>
                {{ Form::submit('Inscription', array("class" => "btn btn-primary pull-right")) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$("#state").change(function() {
		if($(this).val() == 'external' || $(this).val() == 'student')
			$("#motivation").fadeIn();
		else
			$("#motivation").fadeOut();
		if($(this).val() == 'student' || $(this).val() == 'association' || $(this).val() == 'company')
			$("#entityName").fadeIn();
		else
			$("#entityName").fadeOut();
	})
</script>
@stop
