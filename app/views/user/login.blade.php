@extends('layouts.master')
@section('pageName')
Connexion
@stop
@section('sidebar')
@parent

@stop

@section('content')
<ul>
    <?php
    foreach($errors->getMessages() as $msg)
    {
        ?><li><?= $msg[0]; ?></li><?php
    }
    ?>
</ul>
    <div class="col-md-8 col-sm-8" style="margin-bottom: 20px;">
        <div class="box-title-container">
            <h3 class="box-title">
                <span><img src="{{ asset('front/css/images/actu_ico.png') }}" class="icon"/> Connectez-vous</span>
            </h3>

            <div class="box">
                <div class="box_content">
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{{ Session::get('error') }}}
                    </div>
                    @endif

                    {{ Form::open(array('action' => 'UserController@postLogin')) }}
                    {{ Form::label('email', 'Email') }}
                    {{ Form::text('email', null, array("class" => "form-control")) }}
                    <br>
                    {{ Form::label('pw', 'Password') }}
                    {{ Form::password('pw', array("class" => "form-control")) }}
                    <br>
                    {{ Form::submit('Connexion', array("class" => "btn btn-info")) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop
