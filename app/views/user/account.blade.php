@extends('layouts.master')
@section('pageName')
    Mon compte
@stop

@section('content')

<div class="col-md-8 col-sm-8" style="margin-bottom: 20px;">
    <div class="row">

        <div class="box-title-container">
            <h3 class="box-title">
                <span><img src="{{ asset('front/css/images/actu_ico.png') }}" class="icon"/> Mes données sensibles</span>
            </h3>

            <div class="box">
                <div class="box_content">
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{{ Session::get('error') }}}
                    </div>
                    @endif

                    {{ Form::open(array('action' => 'UserController@postPassword_update')) }}
                    {{ Form::label('pw', 'Mot de passe actuel') }}
                    {{ Form::password('pw', array("class" => "form-control")) }}
                    <br>
                    {{ Form::label('new', 'Nouveau mot de passe') }}
                    {{ Form::password('new', array("class" => "form-control")) }}
                    <br>
                    {{ Form::label('new_confirm', 'Confirmation') }}
                    {{ Form::password('new_confirmation', array("class" => "form-control")) }}
                    <br>
                    {{ Form::submit('Changer le mot de passe', array("class" => "btn btn-info")) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>

    </div>
    <div class="row">
    <div class="box-title-container">
        <h3 class="box-title">
            <span><img src="{{ asset('front/css/images/actu_ico.png') }}" class="icon"/> Mon avatar</span>
        </h3>

        <div class="box">
            <div class="box_content">
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{ asset($currentUser->avatar) }}" alt="..." class="img-thumbnail">
                    </div>

                    <div class="col-md-9">
                        {{ Form::open(array("action" => "UserController@postAvatar", "files" => true)) }}
                        <div class="form-group">
                            {{ Form::file('avatar', array("class" => "form-control")) }}
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Changer l\'avatar', array("class" => "btn btn-info")) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>

</div>
@stop