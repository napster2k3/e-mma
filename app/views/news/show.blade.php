@extends('layouts.master')
@section('pageName')
News
@stop

@section('content')
<div class="col-md-8 col-sm-8" style="margin-bottom: 20px;">
    <div class="box-title-container">
        <h3 class="box-title"><span><img alt="actu" src="http://e-mma.epitech.eu/wp-content/themes/custom/images/actu_ico.png" class="icon"> L'actualité d'e-mma</span></h3>
    </div>
    <div class="box">
        <div class="box_content">
            <h5 style="display: inline">
                <a href="http://www.e-mma.epitech.eu/?p=348">
                    {{{ $news->title }}}
                </a>
            </h5>
            <h5 class="date">{{ $news->created_at }}</h5>
            <br><br>
            <p style="text-align: justify;">
                <span style="line-height: 1.5em;">
                    {{ $news->content }}
                </span>
            </p>
            <div class="sm_separator"></div>
        </div>
    </div>
</div>
@stop