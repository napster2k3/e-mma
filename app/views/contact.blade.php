@extends('layouts.master')
@section('pageName') Contactez-nous @stop
@section('content')
<div class="col-md-8 col-sm-8" style="margin-bottom: 20px;">
    <div class="box-title-container">
        <h3 class="box-title">
            <span><img src="{{ asset('front/css/images/actu_ico.png') }}" class="icon"/> Contactez-nous</span>
        </h3>

        <div class="box">
            <div class="box_content">
                <form id="contact">
                    <div class="form-group">
                        <label>Prénom</label>
                        <input id="prenom" type="text" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Email:</label>
                        <input required type="email" id="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Message:</label>
                        <textarea required class="form-control" rows="12"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-info pull-right">Nous contacter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $("#contact").submit(function()
    {
        alert('Fonction en cours de développement');
        return false;
    });
</script>
@stop