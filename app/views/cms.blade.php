@extends('layouts.master')

@section('pageName')
{{{ $page->title }}}
@stop
@section('content')
{{ $page->content }}
@stop