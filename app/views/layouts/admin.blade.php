<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>E-mma | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="{{ asset('backend/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset('backend/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="{{ asset('backend/css/morris/morris.css') }}" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="{{ asset('backend/css/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css" />
    <!-- fullCalendar -->
    <link href="{{ asset('backend/css/fullcalendar/fullcalendar.css') }}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="{{ asset('css/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="{{ asset('backend/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('backend/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />

    <!-- jQuery 2.0.2 -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- jQuery UI 1.10.3 -->
    <script src="{{ asset('backend/js/jquery-ui-1.10.3.min.js') }}" type="text/javascript"></script>
    <script src="http://cdn.alloyui.com/2.5.0/aui/aui-min.js"></script>

    <script type="text/javascript" src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            selector: "textarea:not(.textarea-no-styles)",
        });
    </script>
    <script src="{{ asset('backend/js/jquery.leanModal.min.js') }}" type="text/javascript"></script>

    <script src="{{asset('backend/code_editor/lib/codemirror.js')}}"></script>
    <link rel="stylesheet" href="{{asset('backend/code_editor/lib/codemirror.css')}}">
    <script src="{{asset('backend/code_editor/mode/javascript/javascript.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
<a href="index.html" class="logo">
    <!-- Add the class icon to your logo image or logo icon to add the margining -->
    E-mma BO
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
<!-- Sidebar toggle button-->
<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
<div class="navbar-right">
<ul class="nav navbar-nav">

<!-- User Account: style can be found in dropdown.less -->
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="glyphicon glyphicon-user"></i>
        <span> {{{ $currentUser->surname . " " . $currentUser->name }}} <i class="caret"></i></span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header bg-light-blue">
            <img src="{{{ isset($currentUser->avatar) ? asset($currentUser->avatar) : asset('no-avatar.png') }}}" class="img-circle" alt="User Image" />
            <p>
                {{{ $currentUser->surname . " " . $currentUser->name }}}<Developer></Developer>
                <small>Membre depuis {{{ $currentUser->dateStr }}}</small>
            </p>
        </li>
        <!-- Menu Body -->
        <li class="user-body">
           <strong>Role:</strong>
           <span class="pull-right">
               {{{ $currentUser->roles()->first()->name }}}
           </span>
        </li>
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="/" class="btn btn-default btn-flat">Retour au site</a>
            </div>
            <div class="pull-right">
                <a href="/user/logout" class="btn btn-default btn-flat">Deconnexion</a>
            </div>
        </li>
    </ul>
</li>
</ul>
</div>
</nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{{ asset($currentUser->avatar) }}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>Bonjour, {{{ $currentUser->surname }}}</p>

                    <a href="#"><i class="fa fa-circle text-success"></i> En ligne</a>
                </div>
            </div>


            <?php
                $menu = array(
                    "Dashboard" => array(
                        "url" => "/admin/dashboard",
                        "class" => "fa-dashboard"
                    ),
                    "Timeline" => array(
                        "url" => "/admin/timeline",
                        "class" => "fa-eye"
                    ),
                    "News" => array(
                        "url" => "/admin/news",
                        "class" => "fa-th"
                    ),
                    "Users" => array(
                        "url" => "/admin/users",
                        "class" => "fa-user"
                    ),
                     "Events" => array(
                        "url" => "/admin/event",
                        "class" => "fa-calendar"
                    )
                );
            ?>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="">
                    <a href="/admin/dashboard">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/timeline">
                        <i class="fa fa-eye"></i> <span>Timeline</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/news">
                        <i class="fa fa-th"></i> <span>News</span> <small class="badge pull-right bg-green"></small>
                    </a>
                </li>
                <li>
                    <a href="/admin/event">
                        <i class="fa fa-calendar"></i> <span>Events</span> <small class="badge pull-right bg-green"></small>
                    </a>
                </li>
                <li>
                    <a href="/admin/users">
                        <i class="fa fa-users"></i> <span>Users</span> <small class="badge pull-right bg-green"></small>
                    </a>
                </li>
                <li>
                    <a href="/admin/cms">
                        <i class="fa fa-code"></i> <span>Pages (CMS)</span> <small class="badge pull-right bg-green"></small>
                    </a>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('pageName')
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Emma Back Office - </a></li>
                <li class="active">@yield('pageName')</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            @yield('content')

        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->



<!-- Bootstrap -->
<script src="{{ asset('backend/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- Morris.js charts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('backend/js/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
<!-- Sparkline -->
<script src="{{ asset('backend/js/plugins/sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<!-- jvectormap -->
<script src="{{ asset('backend/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}" type="text/javascript"></script>
<!-- fullCalendar -->
<script src="{{ asset('backend/js/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('backend/js/plugins/jqueryKnob/jquery.knob.js') }}" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="{{ asset('backend/js/plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('backend/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{ asset('backend/js/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<!-- Calendar -->
<script src="{{ asset('backend/js/emmaCalendar.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/js/AdminLTE/app.js') }}" type="text/javascript"></script>

</body>
</html>