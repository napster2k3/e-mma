<html lang="fr-FR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://e-mma.epitech.eu/wp-content/themes/custom/images/favicon.ico">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <title>@yield('pageName') | E-mma</title>

    <!-- Bootstrap -->
    <link href="{{ asset('front/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/style.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrap container">
    <header class="hidden-xs">
        <div id="menu" role="navigation">
            <div class="menu-menu-1-container">
                <ul id"menu-menu-1" class="menu">
                <li><a href="/">Accueil</a></li>
                <li><a href="/partenaires">Partenaires</a></li>
                <li><a href="/news">Articles</a></li>
                <li><a href="/events">Evénements</a></li>
                <li><a href="/contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </header>
    <header class="visible-xs">
        <nav class="navbar navbar-default navbar-fixed-top navbackground" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
            <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" style="height: 1px;">
                <ul class="nav navbar-nav">
                     <div class="menu-menu-1-container">
                        <ul id="menu-menu-2" class="menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-144"><a href="/">Accueil</a></li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-145"><a href="/partenaires">Partenaires</a></li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-146"><a href="/news">Articles</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-149"><a href="/events">Evènements</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-163"><a href="/contact">Contact</a></li>
                        </ul>
                    </div>
                </ul>
            </div>
        </nav>
    </header>

    <div class="separator"></div>
    <div class="content">

        @yield('content')

        <div class="side col-md-4 col-sm-4">
            <div class="widget-1 widget-first widget-odd calendar widget_ai1ec_agenda_widget" id="ai1ec_agenda_widget-2"><div class="widget-1 widget-first widget-odd min_box_content">
                    <h5>Utilisateur</h5>
                    <div class="timely ai1ec-agenda-widget-view">
                        <div class="clearfix">
                            @if($currentUser != NULL)

                            <a href="/user/account">- Mon compte</a><br>
                            <a href="/user/logout">- Deconnexion</a><br>
                            @if($currentUser->is('Super Admin') || $currentUser->is('Bureau'))
                            <a style="color:red" href="/admin/dashboard">- Administration</a><br>
                            @endif
                            @else
                                <a href="/user/login">- Connexion</a><br>
                                <a href="/user/register">- Inscription</a>
                            @endif
                        </div><!--/.clearfix-->
                    </div><!--/.ai1ec-agenda-widget-view-->

                </div>
            </div>
            <div class="widget-1 widget-first widget-odd calendar widget_ai1ec_agenda_widget" id="ai1ec_agenda_widget-2"><div class="widget-1 widget-first widget-odd min_box_content">
                    <h5>L'agenda</h5>
                    <div class="timely ai1ec-agenda-widget-view">
                        <div class="clearfix">

                            <p class="ai1ec-no-results">
                                Il n'y a pas d'événements à venir   </p>

                            <p>
                                <a class="btn btn-mini pull-right ai1ec-calendar-link" href="http://www.e-mma.epitech.eu/?page_id=17&amp;ai1ec=">
                                    Voir le calendrier<i class="icon-arrow-right"></i>
                                </a>
                            </p>

                        </div><!--/.clearfix-->
                    </div><!--/.ai1ec-agenda-widget-view-->

                </div>
            </div>


            <div class="widget-3 widget-odd widget_weptile-image-slider-widget" id="weptile-image-slider-widget-3">
                <div class="widget-3 widget-odd min_box_content">
                    <h5>Nos partenaires</h5>
                    <div class="col-lg-12">

                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                   <a href="/partenaires" target="_blank" rel="" class="nivo-imageLink" style="display: block;">
                                        <img src="{{asset('front/css/images/Logo-DigitasLBi.jpg')}}" alt="" title="" style="width: 100%;">
                                    </a>
                                
                                </div>
                                  <div class="item">
                                    <a href="/partenaires" target="_blank" rel="" class="nivo-imageLink" style="display: block;">
                                        <img src="{{asset('front/css/images/logo-supdame.png')}}" alt="" title="" style="width: 100%;">
                                    </a>
                                    
                                  </div>

                                  <div class="item">
                                    <a href="/partenaires" target="_blank" rel="" class="nivo-imageLink" style="display: block;">
                                        <img src="{{asset('front/css/images/Logo-WAX.jpg')}}" alt="" title="" style="width: 100%;">
                                    </a>
                                    
                                  </div>

                                  <div class="item">
                                    <a href="/partenaires" target="_blank" rel="" class="nivo-imageLink" style="display: block;">
                                        <img src="{{asset('front/css/images/logo-epitech.jpg')}}" alt="" title="" style="width: 100%;">
                                    </a>
                                    
                                  </div>

                                  <div class="item">
                                    <a href="/partenaires" target="_blank" rel="" class="nivo-imageLink" style="display: block;">
                                            <img src="{{asset('front/css/images/Logo-GIW.jpg')}}" alt="" title="" style="width: 100%;">
                                        </a>
                                    
                                  </div>
                                                            
                            </div>
                          


                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                              <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>

                            
                        </div>
                       
                     </div>
                </div>
            </div>

            <br />
            <div class="widget-4 widget-even temoignage widget_categoryposts" id="categoryposts-3">
                <div class="widget-4 widget-even min_box_content">
                        <h5 style="margin-top: 130px;">Remerciements</h5>
                        <ul>
                            <li class="cat-post-item">
                                <a class="post-title" href="http://www.e-mma.epitech.eu/?p=147" rel="bookmark" title="Permanent link to Merci!">Merci!</a>
                                <p>Nous voulions dédier cette page à toutes les personnes qui nous ont soutenues depuis la création de...</p>
                            </li>
                        </ul>
                </div>
            </div>
                <br />


            <div class="widget-5 widget-last widget-odd Social_Widget" id="social-widget-2">
                <div class="widget-5 widget-last widget-odd min_box_content">
                    <h5>Retrouver e-mma sur</h5>
                    <div class="socialmedia-buttons smw_center">
                        <a href="https://www.facebook.com/emma.epitech" rel="nofollow" target="_blank">
                            <center>
                                <a href="https://www.facebook.com/emma.epitech">
                                    <img width="48" height="48" src="{{asset('front/css/images/facebook.png')}}" alt="suivez nous sur Facebook" title="suivez nous sur Facebook" class="bounce">
                                </a>

                                <a href="https://twitter.com/EmmaEpitech" target="_blank">
                                    <img width="48" height="48" src="{{asset('front/css/images/twitter.png')}}" alt="suivez nous sur Twitter" title="suivez nous sur Twitter" class="bounce">
                                </a>
                                

                            </center>
                    </div>
                </div>
            </div></div>
        </div>
    </div>
</div>

</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('front/js/bootstrap.min.js') }}"></script>
</body>
</html>