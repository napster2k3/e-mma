<?php

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

namespace App\Models;

class News extends \Eloquent
{
    protected $table = "news";
    protected $fillable = array('title', 'content', 'published', 'author_id');

    public function splitContent()
    {
        if(strlen($this->content) > 200) {
            $this->content = substr($this->content, 0, 200);
            $this->content .= '...';
        }
    }

    public function author()
    {
        return $this->belongsTo('App\Models\User', 'author_id', 'id');
    }

    public static function boot()
    {
        parent::boot();

        self::created(function($news) {
            Timeline::create(array(
                "user_id" => $news->author_id,
                "title" => "a écrit une news !",
                "icon" => "fa-edit",
                "content" => "<strong>Titre:</strong> " . \HTML::entities($news->title) ."<br><br><a href=\"/admin/news/edit/".$news->id."\" class=\"btn btn-info\">Voir la news</a>",
                "color" => "bg-blue"
            ));
        });

        self::updated(function($news) {
            Timeline::create(array(
                "user_id" => \Auth::user()->id,
                "title" => "a mis à jour une news !",
                "icon" => "fa-edit",
                "content" => "<strong>Titre:</strong> " . \HTML::entities($news->title) ."<br><br><a href=\"/admin/news/edit/".$news->id."\" class=\"btn btn-info\">Voir la news</a>",
                "color" => "bg-orange"
            ));
        });

        self::deleted(function($news) {
            Timeline::create(array(
                "user_id" => \Auth::user()->id,
                "title" => "a supprimé une news ! ",
                "icon" => "fa-times-circle",
                "content" => "News #" . $news->id,
                "color" => "bg-red"
            ));
        });
    }
}
