<?php  
namespace App\Models;
/**
 * Guillaume Lambert - <guillaume1.lambert@epitech.eu>
 * Project: E-MMA
 */

class Event extends \Eloquent
{
	protected $table = 'event';
	protected $fillable = array('title' , 'description' , 'author_id'  , 'published' , 'author_email' , 'start' , 'end' , 'place_name' , 'adresse');

	public function author()
	{
		return $this->belongsTo('App\Models\User', 'author_id', 'id');
	}

    /**
     * Model Event Manager
     */

    public static function boot()
	{
		parent::boot();

		self::created(function($event) {
			Timeline::create(array(
				"user_id" => $event->author_id,
				"title" => "a crée un event !",
				"icon" => "fa-edit",
				"content" => "<strong>Titre:</strong> " . \HTML::entities($event->title) ."<br><br><a href=\"/admin/event/edit/".$event->id."\" class=\"btn btn-info\">Voir l'event</a>",
				"color" => "bg-blue"
				));
		});

		self::updated(function($event) {
			Timeline::create(array(
				"user_id" => \Auth::user()->id,
				"title" => "a mis à jour un event !",
				"icon" => "fa-edit",
				"content" => "<strong>Titre:</strong> " . \HTML::entities($event->title) ."<br><br><a href=\"/admin/event/edit/".$event->id."\" class=\"btn btn-info\">Voir l'event</a>",
				"color" => "bg-orange"
				));
		});

		self::deleted(function($event) {
			Timeline::create(array(
				"user_id" => \Auth::user()->id,
				"title" => "a supprimé un event ! ",
				"icon" => "fa-times-circle",
				"content" => "Event #" . $event->id,
				"color" => "bg-red"
				));
		});
	}

}