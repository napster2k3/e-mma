<?php

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

class Page extends \Eloquent
{

    protected $table = "pages";
    protected $fillable = array('name', 'title', 'content', 'slug', 'roles');

    public static function boot()
    {
        parent::boot();
    }
}
