<?php

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

namespace App\Models;

class Timeline extends \Eloquent
{
    protected $table = "timeline";
    protected $fillable = array('title', 'content', 'icon', 'color', 'user_id');

    /**
     * belongsTo relation
     * @return mixed
     */

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * Return the elapsed time from a date.
     * @param $time
     * @return string
     */

    public static function dateToStr($time)
    {
        $time = time() - $time;

        $tokens = array (
            31536000 => 'an',
            2592000 => 'mois',
            604800 => 'semaine',
            86400 => 'jour',
            3600 => 'heure',
            60 => 'minute',
            1 => 'seconde'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits . ' ' .$text.(($numberOfUnits>1)?'s':'');
        }
    }
}
