<?php

/**
 * Camille Greselle - <camille.greselle@epitech.eu>
 * Project: E-MMA
 */

namespace App\Models;

use Toddish\Verify\Models\User as VerifyUser;

class User extends VerifyUser
{
    /** @var array Validation Rules */

    private $rules = array(

        "register" => array(
            "email" => "required|email|unique:users",
            "pw" => "required",
            "pw_confirm" => "required"
        ),
        "login" => array(
            "email" => "required|email",
            "pw" => "required"
        )
    );

    /** hasMany news relation */

    public function news()
    {
        return $this->hasMany('App\Models\News', 'author_id', 'id');
    }

    /** Approve an User */

    public function approveUser($user)
    {
        if(!$user->verified) {
            $user->verified = true;
            $user->save();
            return true;
        }
        return false;
    }

    /** Boot Option (Catching Model Event) */

    static public function boot()
    {
        parent::boot();

        self::created(function($user) {
            Timeline::create(array(
                "title" => "Nouvel Utilisateur !",
                "icon" => "fa-user",
                "content" => "Un utilisateur s'est enregistré, il est placé en attente d'approbation ! <a href='/admin/users/profile/" . $user->id ."'>Voir le profil</a>",
                "color" => "bg-green"
            ));
        });

        self::deleted(function($user) {
            Timeline::create(array(
                "user_id" => \Auth::user()->id,
                "title" => "a supprimé / décliner l'application de " . \HTML::entities($user->surname) . " " . \HTML::entities($user->name) . " !",
                "icon" => "fa-user",
                "content" => "Un utilisateur s'est enregistré, il est placé en attente d'approbation ! <a href='/admin/users/profile/'" . $user->id .">Voir le profil</a>",
                "color" => "bg-red"
            ));
        });
    }

    public function getRules($rules) { return isset($this->rules[$rules]) ? $this->rules[$rules] : "rules does not exist" ; }
}
