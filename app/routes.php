<?php

/** viewInjection Inclusion */

require_once(app_path() . "/viewInjection.php");

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::controller('user', 'UserController');

/** Admin Controller */

Route::controller('admin/cms', 'AdminCMSController');
Route::controller('admin/acl', 'AdminACLController');
Route::controller('admin/users', 'AdminUserController');
Route::controller('admin/event', 'AdminEventController');
Route::controller('admin/news', 'App\Controller\Admin\AdminNewsController');
Route::controller('admin', 'App\Controller\Admin\AdminController');

/** CRSF Protection */

Route::filter('csrf', function()
{
    if (Request::forged()) return Response::error('500');
});

Route::get('contact', 'HomeController@getContact');
Route::controller('events', 'EventController');
Route::get('news/', 'NewsController@getIndex');
Route::get('news/{slug}', 'NewsController@showNews');
Route::get('/', 'HomeController@getIndex');
Route::get('/{slug}', 'HomeController@getPage');
