<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pages', function($table)
        {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('title', 100);
            $table->text('content');
            $table->string('slug', 300);
            $table->string('roles', 500)->nullable();
            $table->integer('views');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
