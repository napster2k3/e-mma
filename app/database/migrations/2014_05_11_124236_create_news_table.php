<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('news', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title', 255);
            $table->text('content');
            $table->unsignedInteger('author_id');
            $table->string('img', 255)->nullable();
            $table->string('slug', 255);
            $table->boolean('published');
            $table->timestamps();
            $table->foreign('author_id')->references('id')->on('users');
            echo "Success: Created News Table\n";
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
