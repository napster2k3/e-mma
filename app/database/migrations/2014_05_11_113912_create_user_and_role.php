<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Toddish\Verify\Models\Role;
use App\Models\User;

class CreateUserAndRole extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Creating Default Role

		$role = new Role;
        $role->name = "User";
        $role->level = 1;
        $role->save();

       	$role = new Role;
		$role->name = "Super Admin";
		$role->level = 10;
		$role->save();

        $role = new Role;
        $role->name = 'Bureau';
        $role->level = 8;
        $role->save();

        // Creating User with right

        $user = new User;
        $user->email = "admin@admin.com";
        $user->password = "admin";
        $user->verified = 1;
        $user->disabled = 0;
        $user->username = 'gresel_c';
        $user->save();
        $user->roles()->sync(array(2));
        echo "Migration done ! \n Created user admin@admin.com : admin with SuperAdmin right!\n Let's take a beer then code bitch <3\n";
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
