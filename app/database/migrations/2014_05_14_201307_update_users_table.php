<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('users', function($table)
        {
            $table->string('name', 100);
            $table->string('surname', 100);
            $table->string('state', 20);
            $table->text('disabled_reason')->nullable();
            $table->text('motivation')->nullable();
            $table->string('entity', 100)->nullable();
            $table->string('avatar')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
