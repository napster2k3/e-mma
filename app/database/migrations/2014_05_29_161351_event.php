<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Event extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event' ,  function($table)
		{
			$table->increments('id');
			$table->string('title', 255);
			$table->unsignedInteger('author_id');
			$table->string('author_email');
			$table->string('description' , 255);
			$table->string('img', 255)->nullable();
			$table->string('slug', 255);
			$table->boolean('published');
			$table->date('start');
			$table->date('end');
			$table->string('place_name');
			$table->string('adresse');
			$table->foreign('author_id')->references('id')->on('users');
			$table->foreign('author_email')->references('email')->on('users');
            $table->timestamps();
			echo "Success: Created Event Table MoThA fUcKa bwa bwa bap\n";
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
