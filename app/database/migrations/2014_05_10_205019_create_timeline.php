<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Timeline;

class CreateTimeline extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('timeline', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title', 255);
            $table->text('content');
            $table->string('icon', 100);
            $table->string('color', 10);
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            echo "Success: Created Timeline Table\n";
        });

        Timeline::create(array(
           "title" => "<a href=\"#\">Root User</a>",
            "content" => "Initialisation E-MMA: OK<br>The website has been installed !",
            "color" => "bg-green",
            "icon" => "fa-check-square-o"
        ));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
